const fs = require('fs');

const path = './demo';

function createAndDeleteJSONFile(){
    fs.mkdir(path,{recursive:true},(err) =>{
        for(let index = 0 ; index <  3 ; index++){
            let newPath = `./demo/${index}.json`;
            let data = `file ${index} created successfully`;
            fs.writeFileSync(newPath,data,{encoding:'utf-8'});
            console.log(data);
            fs.unlinkSync(newPath);
            console.log(`File ${index} deleted successfully`);
            
        }
    })
    
}

module.exports = createAndDeleteJSONFile;