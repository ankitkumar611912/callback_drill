/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/


const fs = require('fs');
const path = './lipsum_1.txt'

function implementationOfProblemTwo(){
    fs.readFile(path,"utf-8" , (err,data) => {
        if(err){
            console.error(error);
        }else{
            let newData = data.toUpperCase();
            let path1 = './upperCase.txt';
            let path2 = './filenames.txt';
            fs.writeFile(path1,newData,(err1,data1) => {
                if(err1){
                    console.log(err1);
                }else{
                    fs.writeFile(path2,'upperCase.txt ',(err2,data2) => {
                        if(err2){
                            console.error(err2);
                        }
                    })
                    fs.readFile(path1,"utf-8",(err3,data3) => {
                        if(err3){
                            console.error(err3);
                        }else{
                            let str = data3;
                            str = str.toLowerCase();
                            let sentences = str.split('.');
                            sentences.forEach((sentence) => {
                                fs.writeFile('lowerCase.txt', sentence.trim() +'\n' , {flag: "a"}, (err) => {
                                  if (err) {
                                    console.log("error occured: ", err);
                                  }
                                });
                            });
                        }
                        fs.writeFile('./filenames.txt','lowerCase.txt ', {flag: "a"} , (err4,data4) => {
                            if(err4){
                                console.error(err4)
                            }
                        })
                        fs.readFile('./lowerCase.txt', "utf-8" ,(err5,data5) =>{
                            if(err5){
                                console.error(err5);
                            }else{
                                let newStr = data5;
                                let newArr = newStr.split('\n');
                                
                                newArr = newArr.sort();
                                newArr.forEach((sentence) => {
                                    if(sentence !== ''){
                                        fs.writeFile('./sortContent.txt',sentence + '\n', {flag : "a"} , (err6,data6) =>{
                                            if(err6){
                                                console.error(err6);
                                            }
                                        })
                                    }
                                    
                                })
                                
                            }
                            
                        })
                        fs.writeFile('./filenames.txt', 'sortContent.txt', {flag: "a"} , (err7,data7) =>{
                            if(err7){
                                console.error(err7);
                            }
                        })
                        fs.readFile('filenames.txt', "utf-8" , (filerror,filedata) =>{
                            if(filerror){
                                console.error(filerror);
                            }else{
                                
                                let filePath = filedata.split(' ');
    
                                for(let index = 0 ; index < filePath.length ; index++){
                                    fs.unlink(filePath[index],(unlinkerr,unlinkdata) =>{
                                        if(unlinkerr){
                                            console.error(unlinkerr);
                                        }
                                    })
                                }
                            }
                        })
                    })
                    
                }
                
                
            })
        }
    })
    
}

module.exports = implementationOfProblemTwo;